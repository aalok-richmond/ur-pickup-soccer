### A repository to host materials for the *UR Pick-up Soccer* club at the University of Richmond

**Repo structure:**
```bash
.
├── constitution
│   └── constitution.tex
├── graphics
│   └── logo
├── LICENSE
└── README.md
```

To access the compiled `constitution.pdf` file, simply visit [this link](https://gitlab.com/aalok-richmond/ur-pickup-soccer/-/jobs/artifacts/master/raw/constitution.pdf?job=build_pdf).
